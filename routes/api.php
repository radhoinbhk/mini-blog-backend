<?php

use App\Http\Controllers\PosteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("login", [UserController::class, 'authenticate']);
Route::post("register", [UserController::class, 'createUser']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    //All secure URL's
    Route::post("addPost", [PosteController::class, 'createPoste']);
    Route::post("addComment", [PosteController::class, 'createComment']);
    Route::get("getComments/{id}", [PosteController::class, 'getCommentsByPostId']);
    Route::get("getPost", [PosteController::class, 'getAllPost']);
    Route::delete("deletPost/{id}", [PosteController::class, 'deletPostById']);
    Route::delete("deletComment/{id}", [PosteController::class, 'deletCommentById']);
});
