# Mini blog back-end

## Install the dependencies

```
composer ins
npm install

```
## setup database in .env file

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=mini-blogdb
DB_USERNAME=root
DB_PASSWORD=
```

## Run migrations + seeding

`php artisan migrate --seed`

## Setup in webserver and /etc/hosts

## Finally, serve the application.

`php artisan serve`

