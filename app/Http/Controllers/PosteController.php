<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;

use function PHPSTORM_META\map;

class PosteController extends Controller
{
    public function createPoste(Request $request)
    {
        $user = User::find($request->user_id);
        $post = new Post();
        $post->statut = $request->statut;
        $post->created_date = $request->created_date;
        $user->post()->save($post);
        if (!$post) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        }
        return response('post has been created successfully!', 200);
    }

    public function createComment(Request $request)
    {
        $user = User::find($request->user_id);
        $post = Post::find($request->post_id);
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->created_date = $request->created_date;
        $comment->post_id = $post->id;
        $user->comments()->save($comment);
        if (!$user) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        }
        return response('comment has been saved successfully!', 200);
    }

    public function getCommentsByPostId($id)
    {
        $comments = Post::find($id)->comments;
        $response = $comments->map(function ($comment) {
            $obj = $comment;
            $obj->user = $this->getUserById($comment->user_id);
            return $obj;
        });

        if (!$response) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        }
        return response($response, 200);
    }

    protected function getUserById($id)
    {
        $user = User::find($id);
        return $user;
    }

    protected function getExistingPost()
    {
        $allPost = Post::all('*');
        $response = $allPost->map(function ($post) {
            $obj = $post;
            $obj->user = $this->getUserById($post->user_id);
            $comments = $this->getCommentsByPostId($post->id)->original;
            $obj->comments = $comments;
            return $obj;
        });
        return $response;
    }

    public function getAllPost()
    {
        $allPost = $this->getExistingPost();
        if (!$allPost) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        }
        return response($allPost, 200);
    }

    public function deletPostById($id)
    {
        $deletPost = Post::find($id);
        $deletPost->delete();
        if (!$deletPost) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        }
        $existingPost = $this->getExistingPost();
        return response($existingPost, 200);
    }

    public function deletCommentById($id)
    {
        $deletComment = Comment::find($id);
        $deletComment->delete();
        if (!$deletComment) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        }
        $existingPost = $this->getExistingPost();
        return response($existingPost, 200);
    }
}
