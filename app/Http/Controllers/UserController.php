<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    
    public function authenticate(Request $request)
    {
        $reqData = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if ($this->validatorAuth($reqData)->fails()) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        } else {
            $user = User::where('email', $request->email)->first();
            // print_r($data);
            if (!$user || !Hash::check($request->password, $user->password)) {
                return response([
                    'message' => ['Bad Request']
                ], 404);
            }

            $token = $user->createToken('my-app-token')->plainTextToken;

            $response = [
                'user' => $user,
                'token' => $token
            ];
            return response($response, 200);
        }
    }

    public function createUser(Request $request)
    {
        $user = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
        ];
        if ($this->validator($user)->fails()) {
            return response([
                'message' => ['Bad Request']
            ], 400);
        } else {
            $createdUser = DB::table('users')->insert([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            if (!$createdUser) {
                return response([
                    'message' => ['Bad Request']
                ], 400);
            }
            return response('User Created', 200);
        }
    }

    protected function validatorAuth(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }
}
